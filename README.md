# # Bootstrap for Genesis

## Introduction

Bootstrap for Genesis is a genesis child theme which integrates [Bootstrap](http://getbootstrap.com/). It uses Gulp to handle tasks, configuration and lint files.

## Installation Instructions

1. Upload the Bootstrap for Genesis theme folder via FTP to your wp-content/themes/ directory. (The Genesis parent theme needs to be in the wp-content/themes/ directory as well.)
2. Go to your WordPress dashboard and select Appearance.
3. Activate the Bootstrap for Genesis theme.
4. Inside your WordPress dashboard, go to Genesis > Theme Settings and configure them to your liking.

## Building from Source

1. Install [Git](https://git-scm.com/).
2. Clone the repository to your local machine.
3. Install [Node](https://nodejs.org/en/).
4. Install [Gulp](https://gulpjs.com/) globally.
5. Run `npm install` to install dependencies through terminal/CLI program.
6. Replace site url in line `49` of `gulpfile.js` to your local development URL(e.g. http://bootstrap.test).
7. Run `gulp` through your favorite CLI program.

## Features

1. Bootstrap 4
2. Bootstrap components
  * Comment Form
  * Search Form
  * Jumbotron
  * Navbar
3. Sass
4. Gulp
5. Footer Widgets(modified to add bootstrap column classes based on the number of widget areas)
6. Additional Widget Areas
  * Home Featured(jumbotron)
7. TGM Plugin Activation Support
8. Multi-level dropdown menus using [SmartMenus](http://www.smartmenus.org/) Bootstrap Addon

## Todos

- [ ] Integrate Genesis Theme Setup API
- [ ] Integrate Genesis Configuration API
- [ ] Integrate AMP Support
- [ ] Gutenberg Support
- [ ] Remove SmartMenus support or move to separate plugin
- [ ] Remove TGM Plugin Activation Support
- [ ] Use Git Submodule for WP Bootstrap Navwalker
- [ ] Code cleanup and bug fixes

## Credits

Without these projects, this theme wouldn’t be where it is today.

* [Genesis Framework](http://my.studiopress.com/themes/genesis/)
* [Bootstrap](http://getbootstrap.com)
* [Sass](http://sass-lang.com/)
* [Gulp](http://gulpjs.com/)
* [TGM Plugin Activation](http://tgmpluginactivation.com/)
* [WP Bootstrap Navwalker](https://github.com/twittem/wp-bootstrap-navwalker)
* [Bootstrap Genesis](https://github.com/salcode/bootstrap-genesis)
* [Bones for Genesis 2.0 with Bootstrap integration](https://github.com/jer0dh/bones-for-genesis-2-0-bootstrap)
* [SmartMenus Bootstrap Addon](http://www.smartmenus.org/)

---

## TDR Notes

* Review and set Bootstrap variables in `/assets/scss/supporting/variables-bootstrap-core-override.scss`
* All custom style partials `@import` from `/assets/scss/style.scss`
* Save custom style partials in `/assets/scss/custom/`
* Add custom javascript in `/assets/js/source/app.js`

**Recommended Plugins**

* [Advanced Custom Fields – WordPress plugin | WordPress.org](https://wordpress.org/plugins/advanced-custom-fields/)
* [ACF Theme Code for Advanced Custom Fields – WordPress plugin | WordPress.org](https://wordpress.org/plugins/acf-theme-code/)
* [Advanced Bootstrap Blocks – WordPress plugin | WordPress.org](https://wordpress.org/plugins/advanced-bootstrap-blocks/)
* [Gravity Forms | The Best WordPress Form Plugin | Form Builder](https://www.gravityforms.com/)
* [Google XML Sitemaps – WordPress plugin | WordPress.org](https://wordpress.org/plugins/google-sitemap-generator/)
* [Safe SVG – WordPress plugin | WordPress.org](https://wordpress.org/plugins/safe-svg/)
* [Duplicate Post – WordPress plugin | WordPress.org](https://wordpress.org/plugins/duplicate-post/)
* [Show Current Template – WordPress plugin | WordPress.org](https://wordpress.org/plugins/show-current-template/)
* [Genesis Simple Hooks – WordPress plugin | WordPress.org](https://wordpress.org/plugins/genesis-simple-hooks/)
* [Genesis Visual Hook Guide – WordPress plugin | WordPress.org](https://wordpress.org/plugins/genesis-visual-hook-guide/)
